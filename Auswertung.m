
%% Auswertung
% Information zur Nomenklatur S100_SzOn_AP9999 beinhaltet 3
% Parametervariationen S100 bedeutut BESS hat 100kWh Kapazit?t C=1, SzOn
% bedeutet 20 Szenarien wurden optimiert um Grid und CHP festzulegen bevor
% in der zweiten Runde dann observed values optimiert wurden. Bei SzOff
% wurden in der ersten Runde bereits die Observed values optimiert. Dies
% ist quasi die optimale L?sung (Ausgleichsenergie nicht notwendig). FC
% bedeutut an dieser STelle, dass in der ersten Runde allein der Forecast
% verwendet wurde, bevor in der zweiten Runde dann gegen den observed value
% gemessen wird. Dies ist quasi der direkte Vergleich wendet man keine
% Szenariooptimierung an, sondern optimiert auf den Punktforecast alleine.
% Der letzte Parameter AP9999 ist der Ausgleichspreis. Dieser wird variiert
% zwischen 200, 300 und 999999 EUR/kWh.


folderold=pwd;
foldername=strcat('Simulation-RMSToch-_17-08-22_18-28');
folder=strcat(pwd,'/',foldername);
cd(folder);
load('Sim_1_6'); load('Sim_2_6'); load('Sim_3_6'); load('Sim_4_6'); load('Sim_5_6'); load('Sim_6_6');
S100_SzOn_AP9999=Sim_1_6;
S100_SzOn_AP300=Sim_2_6;
S100_SzOn_AP200=Sim_3_6;
S100_SzOff_AP9999=Sim_4_6;
S100_SzOff_AP300=Sim_5_6;
S100_SzOff_AP200=Sim_6_6;
cd(folderold);

folderold=pwd;
foldername=strcat('Simulation-RMSToch-_17-08-22_11-12');
folder=strcat(pwd,'/',foldername);
cd(folder);
load('Sim_1_12'); load('Sim_2_12'); load('Sim_3_12'); load('Sim_4_12'); load('Sim_5_12'); load('Sim_6_12');
load('Sim_7_12'); load('Sim_8_12'); load('Sim_9_12'); load('Sim_10_12'); load('Sim_11_12'); load('Sim_12_12');
S200_SzOn_AP9999=Sim_1_12;
%S400_SzOn_AP9999=Sim_2_12;
S200_SzOn_AP300=Sim_3_12;
%S400_SzOn_AP300=Sim_4_12;
S200_SzOn_AP200=Sim_5_12;
%S400_SzOn_AP200=Sim_6_12;
S200_SzOff_AP9999=Sim_7_12;
%S400_SzOff_AP9999=Sim_8_12;
S200_SzOff_AP300=Sim_9_12;
%S400_SzOff_AP300=Sim_10_12;
S200_SzOff_AP200=Sim_11_12;
%S400_SzOff_AP200=Sim_12_12;
cd(folderold);

foldername=strcat('Simulation-RMSToch-_17-09-05_16-50');
folder=strcat(pwd,'/',foldername);
cd(folder);
load('Sim_1_9'); load('Sim_2_9'); load('Sim_3_9'); load('Sim_4_9'); load('Sim_5_9'); load('Sim_6_9');
load('Sim_7_9'); load('Sim_8_9'); load('Sim_9_9');
S100_FC_AP200 =Sim_1_9;
S200_FC_AP200 =Sim_2_9;
S400_FC_AP200 =Sim_3_9;
S100_FC_AP300  =Sim_4_9;
S200_FC_AP300  =Sim_5_9;
S400_FC_AP300  =Sim_6_9;
S100_FC_AP9999  =Sim_7_9;
S200_FC_AP9999  =Sim_8_9;
S400_FC_AP9999  =Sim_9_9;

cd(folderold);

foldername=strcat('Simulation-RMSToch-_17-08-25_17-15');
folder=strcat(pwd,'/',foldername);
cd(folder);
load('Sim_1_6'); load('Sim_2_6'); load('Sim_3_6'); load('Sim_4_6'); load('Sim_5_6'); load('Sim_6_6');

S400_SzOn_AP9999    = Sim_1_6;
S400_SzOn_AP300     = Sim_2_6;
S400_SzOn_AP200     = Sim_3_6;
S400_SzOff_AP9999   = Sim_4_6;
S400_SzOff_AP300    = Sim_5_6;
S400_SzOff_AP200    = Sim_6_6;

cd(folderold);




Resultplots_Szen(S100_SzOn_AP9999,1)
Resultplots_Szen(S100_SzOff_AP9999,1)
Resultplots_Szen(S100_FC_AP9999,1)

%% Plot PV Data und Betrachtung erster Simulation
Sim=S100_SzOn_AP9999;
par=Sim.par;
n_D     =Sim.par.T_total; %Anzahl Tage die Simuliert wurden
n_SZ    =Sim.par.S; %Anzahl Szenarien

PV_t_s=[]; PVforecast_t=[]; PVobserved_t =[];
for d=1:n_D
    PV_t_s          =[PV_t_s', Sim.PVDATA.perday(d).PV_t_s(1:24,:)']';
    PVforecast_t    =[PVforecast_t', Sim.PVDATA.perday(d).PVforecast_t(1:24,1)']';
    PVobserved_t    =[PVobserved_t', Sim.PVDATA.perday(d).PVobserved_t(1:24,1)']'; 
end

figure
subplot(3,1,1)
grid on
hold on
stairs(PVforecast_t);
stairs(PVobserved_t);
stairs(min(PV_t_s')');
stairs(max(PV_t_s')');

legend('PV-forecast','PV-observed','min of all Scenarios','max of all Scenarios', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(3,1,2)
title('Prognoseabweichung einzelner Scenarien in kw')
stairs(PV_t_s-repmat(PVforecast_t,1,n_SZ));
grid on
hold on
name=[];
for sz=1:n_SZ
    name=[name {strcat('PV-Sz# ', num2str(sz))}];
end
legend(name, 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(3,1,3)
title('Prognoseabweichung Messdaten in kw')
grid on
hold on
stairs(PVobserved_t-PVforecast_t);
stairs(min((PV_t_s-repmat(PVforecast_t,1,n_SZ))')');
stairs(max((PV_t_s-repmat(PVforecast_t,1,n_SZ))')');



legend('Observed', 'Min all Scenarios', 'Max all Scenarios', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

%% Auswertung ?ber Storage verlauf 100 kWh

figure

subplot(4,1,1)
grid on
hold on
stairs(PVforecast_t(1:72));
stairs(PVobserved_t(1:72));
legend('forecast','observed', 'Location','northeast','Orientation','horizontal')
ylabel('Power in kW');
xlim([0 72]);
set(gca,'XTick', [0:4:72])
set(gca,'XTickLabel', [0:4:72])
title('PV forecast')

% subplot(5,1,2)
% hold all;
% grid on;
% title('Energy exchanged with the grid')
% stairs(S100_SzOn_AP9999.res.P_Grid_d_T-S100_SzOn_AP9999.res.P_Grid_s_T);
% stairs(S100_FC_AP9999.res.P_Grid_d_T-S100_FC_AP9999.res.P_Grid_s_T);
% stairs(S100_SzOff_AP9999.res.P_Grid_d_T-S100_SzOff_AP9999.res.P_Grid_s_T);
% stairs(S100_SzOn_AP300.res.P_Grid_d_T-S100_SzOn_AP300.res.P_Grid_s_T);
% %stairs(I.Pres_t_s(:,sz));
% legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
% ylabel('in kW');
% xlim([0 24*60/par.delta_T*par.T_total]);
% set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
% set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(4,1,2)
hold all;
grid on;
title('Necessary additional grid exchange')
stairs(S100_SzOn_AP9999.res.P_Grid_d_Ausgleich_TS(1:72,1)-S100_SzOn_AP9999.res.P_Grid_s_Ausgleich_TS(1:72,1));
stairs(S100_FC_AP9999.res.P_Grid_d_Ausgleich_TS(1:72,1)-S100_FC_AP9999.res.P_Grid_s_Ausgleich_TS(1:72,1));
stairs(S100_SzOff_AP9999.res.P_Grid_d_Ausgleich_TS(1:72,1)-S100_SzOff_AP9999.res.P_Grid_s_Ausgleich_TS(1:72,1));
stairs(S100_SzOn_AP300.res.P_Grid_d_Ausgleich_TS(1:72,1)-S100_SzOn_AP300.res.P_Grid_s_Ausgleich_TS(1:72,1));
legend('SO','FO', 'ideal', 'SO-300','Orientation','horizontal');
ylabel('Power in kW');
xlim([0 72]);
set(gca,'XTick', [0:4:72])
set(gca,'XTickLabel', [0:4:72])

subplot(4,1,3)
hold all;
grid on;
title('PV curtailment')
stairs(S100_SzOn_AP9999.res.P_Curt_PV_TS(1:72,1));
stairs(S100_FC_AP9999.res.P_Curt_PV_TS(1:72,1));
stairs(S100_SzOff_AP9999.res.P_Curt_PV_TS(1:72,1));
stairs(S100_SzOn_AP300.res.P_Curt_PV_TS(1:72,1));
ylabel('Power in kW');
xlim([0 72]);
set(gca,'XTick', [0:4:72])
set(gca,'XTickLabel', [0:4:72])

subplot(4,1,4)
hold all;
grid on;
title('Energy stored')
ylabel('Energy in kWh');
ylim([0 par.ESS.E_max]);
plot(S100_SzOn_AP9999.res.E_ESSstore_T_1S(1:72,1));
plot(S100_FC_AP9999.res.E_ESSstore_T_1S(1:72,1));
plot(S100_SzOff_AP9999.res.E_ESSstore_T_1S(1:72,1));
plot(S100_SzOn_AP300.res.E_ESSstore_T_1S(1:72,1));
xlim([0 72]);
set(gca,'XTick', [0:4:72])
set(gca,'XTickLabel', [0:4:72])
xlabel('Time in h')

%% Auswertung ?ber Storage verlauf 400 kwH Storage

figure
subplot(5,1,1)
grid on
hold on
stairs(PVforecast_t);
stairs(PVobserved_t);
legend('PV-forecast','PV-observed', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])


subplot(5,1,2)
hold all;
grid on;
title('Energy exchanged with the grid')
stairs(S400_SzOn_AP9999.res.P_Grid_d_T-S400_SzOn_AP9999.res.P_Grid_s_T);
stairs(S400_FC_AP9999.res.P_Grid_d_T-S400_FC_AP9999.res.P_Grid_s_T);
stairs(S400_SzOff_AP9999.res.P_Grid_d_T-S400_SzOff_AP9999.res.P_Grid_s_T);
stairs(S400_SzOn_AP300.res.P_Grid_d_T-S400_SzOn_AP300.res.P_Grid_s_T);
%stairs(I.Pres_t_s(:,sz));

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,3)
hold all;
grid on;
title('ben?tigte Auschleichsleistung')
stairs(S400_SzOn_AP9999.res.P_Grid_d_Ausgleich_TS(:,1)-S400_SzOn_AP9999.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(S400_FC_AP9999.res.P_Grid_d_Ausgleich_TS(:,1)-S400_FC_AP9999.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(S400_SzOff_AP9999.res.P_Grid_d_Ausgleich_TS(:,1)-S400_SzOff_AP9999.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(S400_SzOn_AP300.res.P_Grid_d_Ausgleich_TS(:,1)-S400_SzOn_AP300.res.P_Grid_s_Ausgleich_TS(:,1));

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,4)
hold all;
grid on;
title('PV Curtailment')

stairs(S400_SzOn_AP9999.res.P_Curt_PV_TS(:,1));
stairs(S400_FC_AP9999.res.P_Curt_PV_TS(:,1));
stairs(S400_SzOff_AP9999.res.P_Curt_PV_TS(:,1));
stairs(S400_SzOn_AP300.res.P_Curt_PV_TS(:,1));

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,5)
hold all;
grid on;

title('Energy stored')
ylabel('in kWh');
ylim([0 400]);
plot(S400_SzOn_AP9999.res.E_ESSstore_T_1S(:,1));
plot(S400_FC_AP9999.res.E_ESSstore_T_1S(:,1));
plot(S400_SzOff_AP9999.res.E_ESSstore_T_1S(:,1));
plot(S400_SzOn_AP300.res.E_ESSstore_T_1S(:,1));

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');


%% Auswertung ?ber Storage verlauf 200 kwH Storage

figure
subplot(5,1,1)
grid on
hold on
stairs(PVforecast_t);
stairs(PVobserved_t);
legend('PV-forecast','PV-observed', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])


subplot(5,1,2)
hold all;
grid on;
title('Energy exchanged with the grid')
stairs(S200_SzOn_AP9999.res.P_Grid_d_T-S200_SzOn_AP9999.res.P_Grid_s_T);
stairs(S200_FC_AP9999.res.P_Grid_d_T-S200_FC_AP9999.res.P_Grid_s_T);
stairs(S200_SzOff_AP9999.res.P_Grid_d_T-S200_SzOff_AP9999.res.P_Grid_s_T);
stairs(S200_SzOn_AP300.res.P_Grid_d_T-S200_SzOn_AP300.res.P_Grid_s_T);
%stairs(I.Pres_t_s(:,sz));

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,3)
hold all;
grid on;
title('ben?tigte Auschleichsleistung')
stairs(S200_SzOn_AP9999.res.P_Grid_d_Ausgleich_TS(:,1)-S200_SzOn_AP9999.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(S200_FC_AP9999.res.P_Grid_d_Ausgleich_TS(:,1)-S200_FC_AP9999.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(S200_SzOff_AP9999.res.P_Grid_d_Ausgleich_TS(:,1)-S200_SzOff_AP9999.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(S200_SzOn_AP300.res.P_Grid_d_Ausgleich_TS(:,1)-S200_SzOn_AP300.res.P_Grid_s_Ausgleich_TS(:,1));

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,4)
hold all;
grid on;
title('PV Curtailment')

stairs(S200_SzOn_AP9999.res.P_Curt_PV_TS(:,1));
stairs(S200_FC_AP9999.res.P_Curt_PV_TS(:,1));
stairs(S200_SzOff_AP9999.res.P_Curt_PV_TS(:,1));
stairs(S200_SzOn_AP300.res.P_Curt_PV_TS(:,1));

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,5)
hold all;
grid on;

title('Energy stored')
ylabel('in kWh');
ylim([0 200]);
plot(S200_SzOn_AP9999.res.E_ESSstore_T_1S(:,1));
plot(S200_FC_AP9999.res.E_ESSstore_T_1S(:,1));
plot(S200_SzOff_AP9999.res.E_ESSstore_T_1S(:,1));
plot(S200_SzOn_AP300.res.E_ESSstore_T_1S(:,1));

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

legend('Scenario optimized','Forecast optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');