
function [p_DAM_t] = ImportPricedata(delta_T,day,month,year)
% this function imports price data from data base and transfers it to an
% array with time stepsize delta_T. Delta_T is measured in minutes.

%clear all
file=strcat(pwd,'\PriceData\SpotPrices.mat');
load(file)

%year=2016;
%month=8;
%day=1;
%delta_T=5;

name=strcat('SpotPrices', num2str(year));

matrix=table2array(eval(name));
date=year*10000+month*100+day;

[v,index]=max(matrix(:,1)==date);

p_DAM1=matrix(index,2:end);

N=60/delta_T;

for i=1:24
    for n=1:N
       p_DAM_t((i-1)*N+n)=p_DAM1(i);
    end
end
end