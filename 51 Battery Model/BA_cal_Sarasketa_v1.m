function [p_aging, Dtf, Ytf] = BA_cal_Sarasketa_v1(SOC,T,delta_t,C_verl,year_max)
% Taken From Schmalstieg et al.

if SOC < 0
    p_aging=100;
    Dtf=0;
else
    A=165400;
    EA_R=-4148;
    c=0.01;
    z=0.5;
    
    factor=A*exp(EA_R/(T+273.15))*exp(c*SOC*100);
    Dtf=(C_verl*100/factor)^(1/z);
    Ytf=Dtf/365;
    if Dtf>year_max*365
        Dtf=year_max*365;
        Ytf=20;
    end

    p_aging=delta_t/60/24/Dtf;
end

end