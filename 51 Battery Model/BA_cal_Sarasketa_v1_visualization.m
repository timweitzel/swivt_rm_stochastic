clear all
handles=findall(0,'type','figure');
handles.delete;

E_max=10;%in kwh
n_100=6000;

Einv=300*100;

SOC=[0:0.05:1];
T=[25:5:50];
n_SOC=length(SOC);
n_T=length(T);

damage=zeros(n_SOC,n_T);
n=zeros(n_SOC,n_T);

year_max=100;

figure(1);
subplot(1,4,1)
hold on
grid on

ul=0;
delta_t=60;
C_verl=0.2;
for nsoc=1:n_SOC   
    for nT=1:n_T
        cost(nsoc,nT)       =Einv*BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,100);
        damage(nsoc,nT)     =BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,100);
    end
end
name={};
for nT=1:n_T
    plot(SOC*100, cost(:,nT));
    name=[name {strcat('T=', num2str(T(nT)))}];
end
legend(name);

title(strcat('Price per h (100 kWh at 300EUR/kWh without cap)'));
xlabel('SOC in %%');
ylabel('price/h');
ylim([0 0.5]);
%set(gca,'ytick',[0:0.02:0.20]);


%% 15 year cap

subplot(1,4,2)
hold on
grid on
ul=1;
delta_t=60;
C_verl=0.2;
for nsoc=1:n_SOC   
    for nT=1:n_T
        cost(nsoc,nT)       =Einv*BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,15);
        damage(nsoc,nT)     =BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,15);
    end
end

name={};
for nT=1:n_T
    plot(SOC*100, cost(:,nT));
    name=[name {strcat('T= ', num2str(T(nT)))}];
end
legend(name);

title(strcat('Price per h (100 kWh at 300EUR/kWh with 15 year cap)'));
xlabel('SOC in %%');
ylabel('price/h');
ylim([0 0.5]);
%set(gca,'ytick',[0:0.02:0.20]);



%% 20 year cap

subplot(1,4,3)
ul=1;
delta_t=60;
C_verl=0.2;
hold on
grid on

for nsoc=1:n_SOC   
    for nT=1:n_T
        cost(nsoc,nT)       =Einv*BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,20);
        damage(nsoc,nT)     =BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,20);
    end
end

name={};
for nT=1:n_T
    plot(SOC*100, cost(:,nT));
    name=[name {strcat('T= ', num2str(T(nT)))}];
end
legend(name);



title(strcat('Price per h (100 kWh at 300EUR/kWh with 20 year cap)'));
xlabel('SOC in %%');
ylabel('price/h');
ylim([0 0.5]);
%set(gca,'ytick',[0:0.02:0.20]);


%% 25 year cap

subplot(1,4,4)
hold on
grid on
ul=1;
delta_t=60;
C_verl=0.2;
for nsoc=1:n_SOC   
    for nT=1:n_T
        cost(nsoc,nT)       =Einv*BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,25);
        damage(nsoc,nT)     =BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,25);
    end
end

name={};
for nT=1:n_T
    plot(SOC*100, cost(:,nT));
    name=[name {strcat('T= ', num2str(T(nT)))}];
end
legend(name);



title(strcat('Price per h (100 kWh at 300EUR/kWh with 25 year cap)'));
xlabel('SOC in %%');
ylabel('price/h');
ylim([0 0.5]);
%set(gca,'ytick',[0:0.02:0.20]);



%% Damage plot
ul=1;
delta_t=60;
C_verl=0.2;

SOC=[0:0.01:1];
T=[25:5:50];
n_SOC=length(SOC);
n_T=length(T);
damage=zeros(n_SOC,n_T);
n=zeros(n_SOC,n_T);

for nsoc=1:n_SOC   
    for nT=1:n_T
        damage(nsoc,nT)     =BA_cal_Sarasketa_v1(SOC(nsoc),T(nT),delta_t,C_verl,year_max);
    end
end

figure(2)
[X,Y] = meshgrid(SOC*100,T);

surf(X,Y,damage'*100);
hold on
grid on

xl=xlabel('SOC [in %]');
yl=ylabel('T [in ^{\circ} C]');
zl=zlabel('\epsilon_{cal} [in %]');
%zlim([0 0.020]);

xl.FontSize=12;
yl.FontSize=12;
zl.FontSize=12;


%% CAL Aging plot
figure(3)
hold on
grid on

name={};
for t=n_T:-1:1
    plot(SOC*100,damage(:,t)*1000);
    name=[name {strcat('T = ',' ', num2str(T(t)),'^{\circ} C')}];
end
legend(name, 'Location', 'Northwest');

xl=xlabel('SOC [in %]');
yl=ylabel(strcat('\epsilon_{cal} [in ',char(8240),']'));
ylim([0 0.05]);

xl.FontSize=12;
yl.FontSize=12;
set(gcf,'Units','centimeters','PaperPosition',[0 0 8 5],'PaperPositionMode','manual','PaperSize',[8 5])
print(gcf,'-r300','-djpeg', '-opengl','/Users/Timm/Dropbox/Promotion/BA_cal_visualization')


