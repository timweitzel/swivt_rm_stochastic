function [A] = concatenateHMatrix(x_var,A_Input_struct)
    
    A=[];
    for i=1:length(x_var.names)
       A=blkdiag(A, A_Input_struct.(sprintf(x_var.names{i})));
    end
       
end