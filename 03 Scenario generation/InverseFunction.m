function [ Y ] = InverseFunction( F_k, X, K_vect, Zp )
% K Input Z allocated to bin, so vector K has the same Size as Z. Zp is
% evaluated cdf of Z

[n_f, K]=size(F_k);

n_Zp=numel(Zp);

for z=1:n_Zp
   k=K_vect(z);
   F=F_k(:,k);
   for f=1:n_f-1
       if Zp(z)>=F(f) && Zp(z)<=F(f+1)
           Y(z)=X(f);
       end
   end
   if  Zp(z)< F(1)
           Y(z)=X(1);
   end
       if  Zp(z)> F(n_f)
           Y(z)=X(n_f);
       end
end


end

