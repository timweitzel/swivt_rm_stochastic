function [ f,f_q, M, M_red ] = ScenRedMomDistance( Q, P_sz, PV_sz, P_sz_red, PV_sz_red )
%SCENREDMOMDISTANCE Summary of this function goes here
%   Detailed explanation goes here

[n_Sz,T]        = size(P_sz);
[n_Sz_red,T_red]= size(P_sz_red);


f_q=zeros(1,Q);
for q=1:Q
    M(q,:)       =ScenRedMomentum( P_sz,     PV_sz, q);
    M_red(q,:)   =ScenRedMomentum( P_sz_red, PV_sz_red, q);
    f_q(q)       =(1/T * ( sum(M(q,:)) - sum(M_red(q,:)) ))^2;  
end


f=max(f_q);
end

