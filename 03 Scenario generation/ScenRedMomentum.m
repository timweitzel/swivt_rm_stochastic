function [ M ] = ScenRedMomentum( p, P_T, q)
%SCENREDMOMENTUM Summary of this function goes here
%   Detailed explanation goes here
%n_Sz=numel(p);
[n_Sz,n_T]=size(P_T);

for t=1:n_T
   M(t)=0;
   for sz=1:n_Sz
       z=0;
       for sz2=1:n_Sz
           z=z+P_T(sz2,t)*p(sz2);
       end
       M(t)=M(t)+p(sz)*(P_T(sz,t)-z)^q;
   end
end



end

