function [ f_d ] = ScenRedSpaceDistance( PV_sz_red, P_sz_rem, PV_sz_rem )
%SCENREDSPACEDISTANCE Summary of this function goes here
%   Detailed explanation goes here

[n_SZ_rem, T] = size(PV_sz_rem);
[n_SZ_red, T] = size(PV_sz_red);

f_d=0;
for  n1=1:n_SZ_rem
    for n2=1:n_SZ_red
       dist(n2)    =   pdist([PV_sz_rem(n1,:);PV_sz_red(n2,:)],'euclidean');
    end
    f_d=f_d+P_sz_rem(n1)* min(dist);
end
f_d=f_d/T;

end

