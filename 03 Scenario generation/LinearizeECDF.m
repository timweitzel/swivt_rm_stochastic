function f2=LinearizeECDF(f,x,X)

n_x=length(x);
n_X=length(X);

if n_X<=n_x 
   for n=1:n_X
      for n2=1:n_x-1
          if X(n)>=x(n2) && X(n)<x(n2+1)
              f2(n)=(f(n2)+f(n2+1))/2;
          else
              if X(n)<=x(1)
                  f2(n)=f(1);
              end
              if X(n)>=x(n_x)
                  f2(n)=f(n_x);
              end
          end
      end
   end
end

if n_X>=n_x 
   for n=1:n_X
      for n2=1:n_x-1
          if X(n)>=x(n2) && X(n)<x(n2+1)
              f2(n)=(f(n2)+f(n2+1))/2;
          else
              if X(n)<=x(1)
                  f2(n)=f(1);
              end
              if X(n)>=x(n_x)
                  f2(n)=f(n_x);
              end
          end
      end
   end
end

end