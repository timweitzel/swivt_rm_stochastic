function [ Z ] = ( F_k, X, K_vect, X_o )
%TRANSFERFUNCTION Summary of this function goes here
%   Detailed explanation goes here
% K Input Z allocated to bin, so vector K has the same Size as Z. Zp is
% evaluated cdf of Z

[n_f, K]=size(F_k);

n_X_o=numel(Zp);

for z=1:n_X_o
   k=K_vect(z);
   F=F_k(:,k);
   for f=1:n_f-1
       if Zp(z)>=F(f) && Zp(z)<=F(f+1)
           Z(z)=X(f);
       end
   end
end

end

