function [sys_new]=concatenateResults_RM_simplified(sys_old, sys_add)

res1= sys_old.res; 
KPI1= sys_old.KPI; 
I1=   sys_old.I; 
res2= sys_add.res; 
KPI2= sys_add.KPI; 
I2=   sys_add.I_TC; 


res_names=fieldnames(res1);
I_names=fieldnames(I1);
KPI_names=fieldnames(KPI1);

for i=1:numel(res_names)
    if numel( size(res1.(sprintf(res_names{i}))))<=2
        res3.(sprintf(res_names{i}))  =[res1.(sprintf(res_names{i}))' res2.(sprintf(res_names{i}))']';
    end
end
for i=1:numel(KPI_names)
    KPI3.(sprintf(KPI_names{i}))  =KPI1.(sprintf(KPI_names{i}))+KPI2.(sprintf(KPI_names{i}));
end
for i=1:numel(I_names)
    I3.(sprintf(I_names{i}))  =[I1.(sprintf(I_names{i}))' I2.(sprintf(I_names{i}))']';
end

KPI3.OF_Autarchy=(1-KPI3.OF_Aut_z/KPI3.OF_Aut_n)*100;

sys_new=sys_old;
sys_new.res =res3;
sys_new.KPI=KPI3; 
sys_new.I=I3;
end