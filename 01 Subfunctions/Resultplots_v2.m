function resultplots_v2(Sim)
res=Sim.res;
KPI=Sim.KPI;
par=Sim.par;
I=Sim.I;
%handles=findall(0,'type','figure');
%handles.delete;
Nplots=5;
        %% Netzbezug und -Einspeisung 
        figure('Position',[20 100 700 700])
    
        h(1)=subplot(Nplots,2,1);
        hold all;
        grid on;
        stairs(I.pd_t);
        ylabel('in EUR/MWh');
        legend('Price in DAM');
        %plot(zeros(size(result_z_G_demand)), 'k'); % schwarze Null-Linie zeichnen
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% Energy production
        hold off;
        h(2)=subplot(Nplots,2,3);
        hold all;
        grid on;
        stairs(I.PV_t());
        stairs(res.P_Curt_PV_T);
        stairs(res.P_CHP_T);
        legend('P^{PV}_{elec}', 'P^{PV Curtailment}_{elec}', 'P^{CHP}_{elec}');
        ylabel('in kW');
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])

        %% Plot f?r elektrischen Energiespeicher - Inhalt und Leistungen
        hold off;
        h(3)=subplot(Nplots,2,7);
        hold all;
        grid on;
        yyaxis left
        ylabel('in kW');
        ylim([-ceil(max(res.P_ESSdcrg_T)) ceil(max(res.P_ESScrg_T))]);
        stairs(res.P_ESScrg_T-res.P_ESSdcrg_T, 'b-');
        
        yyaxis right
        ylabel('in kWh');
        ylim([0 par.ESS.E_max]);
        plot(res.E_ESSstore_T_1,'r-');

        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
      
        

        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        legend('P^{ESS}_{elec}', 'E^{ESS}_{elec}');

        


        %% Grid
        hold off;
        h(4)=subplot(Nplots,2,5);
        hold all;
        grid on;
        stairs(res.P_Grid_d_T-res.P_Grid_s_T);
        stairs(I.Pres_t);
        legend('Grid','P^{residual}_{elec}' );
        ylabel('in kW');
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        
        %% Total Production
        hold off;
        h(5)=subplot(Nplots,2,9);
        hold all;
        grid on

        prod=(res.P_Grid_d_T  + res.P_ESSdcrg_T + I.PV_t - res.P_Curt_PV_T + res.P_CHP_T)*par.delta_T/60;
        cons=I.PDemand_t*par.delta_T/60 + (res.P_Grid_s_T + res.P_ESScrg_T + res.P_THP_T)*par.delta_T/60;
        y=[prod';cons'];
        bar(y')

        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        legend('Production^{total}_{elec}', 'Consumption^{total}_{elec}');
        xlabel('time');
        ylabel('in kWh')

        
        
        %% W?rme CHP bin
        hold off;
        h(6)=subplot(Nplots,2,4);
        hold all;
        grid on
        stem(res.S_run_CHP_T, '-');  
        stem(2*res.S_sup_CHP_T, '-');
        stem(-1*res.S_off_CHP_T, '-');
        ylim([-1 2]);
        set(gca,'yTick', [-1:1:2])
        set(gca,'YTickLabel',{'off','', 'on','setup'})
        
        
        legend('CHP running', 'CHP setup');
        xlabel('time');
        ylabel('binary')
        
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% W?rme CHP and others
        hold off;
        h(7)=subplot(Nplots,2,6);
        hold all;
        grid on
        name={};
        maxY=0;
        stairs(res.Qd_CHP_T, '-');  
        stairs(res.Qd_HTR_T, '-');
        stairs(res.Qd_THP_T, '-');
        %stairs(res.Qd_delta_T, '-');
  
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        legend('P^{CHP}_{thermal}', 'P^{HTR}_{thermal}', 'P^{THP}_{thermal}');
        xlabel('time');
        ylabel('in kW')
        
        %% W?rme TESS
        hold off;
        h(8)=subplot(Nplots,2,8);
        hold all;
        grid on
        yyaxis left
        stairs(res.Qd_TESS_T, '-');
        xlabel('time');
        ylabel('in kW')
        
        yyaxis right
         plot(res.Q_TESS_T_1, '-');  
        ylabel('in kWh');
        ylim([0 par.TESS.Q_max]);
        legend('P^{TESS}_{thermal}', 'E^{TESS}_{thermal}');
        
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% W?rme Production
        hold off;
        h(9)=subplot(Nplots,2,10);
        hold all;
        grid on
        name={};
        maxY=0;
        prod=res.Qd_CHP_T-res.Qd_TESS_T+res.Qd_HTR_T+res.Qd_THP_T;
        cons=I.QDemand_t;
        y=[prod';cons'];
        bar(y')
        xlim([0 length(y)]);
        
        legend('Production^{total}_{thermal}', 'Consumption^{total}_{thermal}');
        xlabel('time');
        ylabel('in kWh')
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %set(gcf,'Units','centimeters','PaperPosition',[0 0 32 32],'PaperPositionMode','manual','PaperSize',[32 32])
        %print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/SimulationResultPlot')
        
        linkaxes(h,'x')
        
        %% ?bersicht
        figure('Position',[20 100 700 700])
        hold off;
        hold all;
        grid on
        YCOMP=[ KPI.E_ESSlost,  KPI.E_ESSdcrg, KPI.E_ESScrg, -KPI.E_Gridsold, -KPI.E_PVcurt, KPI.E_Gridpur, -KPI.E_THP,  KPI.E_CHP, KPI.E_PVprod,  KPI.E_cons];
        %x=[1,2,3,4,5,6,7,8,9,10,11];
         x=[1,2,3,4,5,6,7,8,9,10];
        ax2=barh(YCOMP);
        set(gca,'YTick',0:1:10); 
        %XTick(ax2,[0 1 2 3 5 6 8 9 10]);
        set(gca,'YTickLabel',{'','P_ESS^{losses}', 'P_ESS^{discharge}','P_ESS^{charge}', 'Grid^{supply}','PV^{curt}', 'Grid^{demand}','THP^{cons}', 'CHP^{prod}','PV^{prod}', 'Demand'})
        xlabel('in kWh');
        for i1=1:numel(YCOMP)
            if YCOMP(i1)>=0
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','left',...
                       'VerticalAlignment','bottom')
            else
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','right',...
                       'VerticalAlignment','bottom')
            end
        end
        
end