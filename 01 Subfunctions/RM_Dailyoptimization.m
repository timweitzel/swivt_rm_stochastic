function [Sim_local] = RM_Dailyoptimization(Sim_local, d, par, opt, period, PVDATA_daily)

% Parameter?bergabe in jede Tagesoptimierung
Sim_local.perday(d).par = Sim_local.par;

if par.T_O_Master>par.T_total-d+1 %Anpassung der Optimierungszeitr?ume, so dass am Ender der Laufzeit nur noch ein Tag optimiret wird
    Sim_local.perday(d).par.T_O     = par.T_total-d+1;
    Sim_local.perday(d).par.T       = 24/(par.delta_T/60)*Sim_local.perday(d).par.T_O; %Optimierungszeitraum in Zeiteinheiten delta_T
    Sim_local.perday(d).par.T_1     = Sim_local.perday(d).par.T + 1;
end

% Inputdefinition f?r jede Tagesoptimierung
%%% Time periods investigated
Sim_local.perday(d).I.datetime    =   Sim_local.perday(d).par.sdate.datetime(period)+(d-1);
l_datetime                        =   Sim_local.perday(d).I.datetime;
Sim_local.perday(d).I.eps_const.n =   eps; %Laufparameter 1:num-1

if opt.THP_COP_var ==1
    Sim_local.perday(d).par.THP.COP= COPperDay(day(l_datetime ),    month(l_datetime ), year(l_datetime ));
end



%Anfangs- und Endbedingungen
if d==1
    Sim_local.perday(d).I.start.Q_TESS_T0      = 0.25 *  Sim_local.par.TESS.Q_max;
    Sim_local.perday(d).I.start.E_ESS_T0       = 0.25 *  Sim_local.par.ESS.E_max; %Anfangswert und gleichzeitig Endwert einer Betrachtung
    Sim_local.perday(d).I.start.S_run_CHP_T0   =0;
    Sim_local.perday(d).I.start.S_run_CHP_T0_num=0; %Anzahl Perioden die das BHKW schon gelaufen ist
    
    Sim_local.perday(d).I.end.Q_TESS_T0        = 0.2 *  Sim_local.par.TESS.Q_max;
    Sim_local.perday(d).I.end.E_ESS_T0         = 0.2 *  Sim_local.par.ESS.E_max;
    
else
    Sim_local.perday(d).I.start.Q_TESS_T0      = Sim_local.perday(d-1).end.Q_TESS_T_1S(1);%Scenario 1 containts observed information
    Sim_local.perday(d).I.start.E_ESS_T0       = Sim_local.perday(d-1).end.E_ESSstore_T_1S(1);
    Sim_local.perday(d).I.start.S_run_CHP_T0   = Sim_local.perday(d-1).end.CHP_run;
    if Sim_local.perday(d-1).end.CHP_run==1 % indieser Abfrage wird ermittelt wie lange das BHKW in der Vorperiode schon gelaufen ist.
        Sim_local.perday(d).I.start.S_run_CHP_T0_num   = 0;
        l=length(Sim_local.perday(d-1).res.S_run_CHP_T);
        l_check=Sim_local.perday(d-1).res.S_run_CHP_T(l-1);
        while  l_check==1 && l>1
            Sim_local.perday(d).I.start.S_run_CHP_T0_num=Sim_local.perday(d).I.start.S_run_CHP_T0_num+1;
            l=l-1;
            if l>1
                l_check=Sim_local.perday(d-1).res.S_run_CHP_T(l-1);
            else
                l_check=0;
            end
        end
    else
        Sim_local.perday(d).I.start.S_run_CHP_T0_num   = 0;
    end
    
    Sim_local.perday(d).I.end.Q_TESS_T0         = Sim_local.par.TESS.Q_max  *0.0;
    Sim_local.perday(d).I.end.E_ESS_T0          = Sim_local.par.ESS.E_max   *0.0;
    
end

%%% energy prices, heat data, pv Production
Sim_local.perday(d).I.pd_t          = Sim_local.par.Price.pmult* ImportPricedata_v2(   Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
Sim_local.perday(d).I.ps_t          = Sim_local.par.Price.Grid_Sell * ones(size(Sim_local.perday(d).I.pd_t));%Sim_local.perday(d).I.pd_t;%

Sim_local.perday(d).I.PV_t_s            = PVDATA_daily.PV_t_s;
Sim_local.perday(d).I.Pr_s              = PVDATA_daily.Pr_s;
Sim_local.perday(d).I.PVforecast_t      = PVDATA_daily.PVforecast_t;
Sim_local.perday(d).I.PVobserved_t      = PVDATA_daily.PVobserved_t;

Sim_local.perday(d).I.PDemand_t     = ImportPDemanddata_v2( Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
Sim_local.perday(d).I.QDemand_t     = ImportQDemanddata_v2( Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
Sim_local.perday(d).I.Pres_t_s      = repmat(Sim_local.perday(d).I.PDemand_t, [1 Sim_local.par.S])   -   Sim_local.perday(d).I.PV_t_s;
Sim_local.perday(d).I.Pres_t_s_pos    = Sim_local.perday(d).I.Pres_t_s(1:par.T_C,:);
Sim_local.perday(d).I.Pres_t_s_pos    = Sim_local.perday(d).I.Pres_t_s_pos(Sim_local.perday(d).I.Pres_t_s_pos>0);
clearvars l_datetime

Sim_local.perday(d).I_TC.datetime       =  Sim_local.perday(d).I.datetime;
Sim_local.perday(d).I_TC.eps_const      =  Sim_local.perday(d).I.eps_const;
Sim_local.perday(d).I_TC.pd_t           =  Sim_local.perday(d).I.pd_t(1:par.T_C);
Sim_local.perday(d).I_TC.ps_t           =  Sim_local.perday(d).I.pd_t(1:par.T_C);
Sim_local.perday(d).I_TC.PV_t_s         =  Sim_local.perday(d).I.PV_t_s(1:par.T_C,:);
Sim_local.perday(d).I_TC.Pres_t_s       =  Sim_local.perday(d).I.Pres_t_s(1:par.T_C,:);
Sim_local.perday(d).I_TC.PDemand_t      =  Sim_local.perday(d).I.PDemand_t(1:par.T_C);
Sim_local.perday(d).I_TC.QDemand_t      =  Sim_local.perday(d).I.QDemand_t(1:par.T_C);

Sim_local.perday(d).scenopt.I= Sim_local.perday(d).I;
Sim_local.perday(d).scenopt.I_TC= Sim_local.perday(d).I_TC;

%% Optimierung
if Sim_local.opt.scenopt==1
    %% Scenopt
    Sim_local.scenopt.opt=Sim_local.opt;
    Sim_local.scenopt.opt.overlay=0;
    Sim_local.scenopt.par = Sim_local.perday(d).par;
    Sim_local.perday(d).scenopt.par  = Sim_local.perday(d).par;
    Sim_local.perday(d).scenopt.res  = RM_OMstoch(  Sim_local.perday(d).scenopt.par,      Sim_local.perday(d).scenopt.I,    Sim_local.scenopt.opt);
    
    %optimiation of observed value
    Sim_local.opt.overlay=1;
    Sim_local.perday(d).par.S=1;
    Sim_local.perday(d).I.scenopt       = Sim_local.perday(d).scenopt.res;
    Sim_local.perday(d).I.PV_t_s        = Sim_local.perday(d).I.PVobserved_t;
    Sim_local.perday(d).I.Pr_s          = 1;
    Sim_local.perday(d).I.Pres_t_s      = Sim_local.perday(d).I.PDemand_t  -   Sim_local.perday(d).I.PV_t_s;
    Sim_local.perday(d).I.Pres_t_s_pos  = Sim_local.perday(d).I.Pres_t_s(1:par.T_C,:);
    Sim_local.perday(d).I.Pres_t_s_pos  = Sim_local.perday(d).I.Pres_t_s_pos(Sim_local.perday(d).I.Pres_t_s_pos>0);
    Sim_local.perday(d).I_TC.PV_t_s     = Sim_local.perday(d).I.PV_t_s(1:par.T_C,:);
    Sim_local.perday(d).I_TC.Pres_t_s   = Sim_local.perday(d).I.Pres_t_s(1:par.T_C,:);
    [Sim_local.perday(d).res  Sim_local.perday(d).end    Sim_local.perday(d).x_var  Sim_local.perday(d).cplex] = RM_OMstoch(  Sim_local.perday(d).par,      Sim_local.perday(d).I,    Sim_local.opt);
else
    %% Scenopt mit nur einem szenario
    %optimiation of observed value
    Sim_local.opt.overlay=0;
    Sim_local.perday(d).par.S=1;
    Sim_local.perday(d).I.PV_t_s        = Sim_local.perday(d).I.PVobserved_t;
    Sim_local.perday(d).I.Pr_s          = 1;
    Sim_local.perday(d).I.Pres_t_s      = Sim_local.perday(d).I.PDemand_t  -   Sim_local.perday(d).I.PV_t_s;
    Sim_local.perday(d).I.Pres_t_s_pos  = Sim_local.perday(d).I.Pres_t_s(1:par.T_C,:);
    Sim_local.perday(d).I.Pres_t_s_pos  = Sim_local.perday(d).I.Pres_t_s_pos(Sim_local.perday(d).I.Pres_t_s_pos>0);
    Sim_local.perday(d).I_TC.PV_t_s     = Sim_local.perday(d).I.PV_t_s(1:par.T_C,:);
    Sim_local.perday(d).I_TC.Pres_t_s   = Sim_local.perday(d).I.Pres_t_s(1:par.T_C,:);
    [Sim_local.perday(d).res  Sim_local.perday(d).end    Sim_local.perday(d).x_var  Sim_local.perday(d).cplex] = RM_OMstoch(  Sim_local.perday(d).par,      Sim_local.perday(d).I,    Sim_local.opt);
    
    % werden hier der vollst?ndigkeit
    % halber ausgef?llt
    Sim_local.scenopt.opt=Sim_local.opt;
    Sim_local.scenopt.opt.overlay=0;
    Sim_local.scenopt.par = Sim_local.perday(d).par;
    Sim_local.perday(d).scenopt.par  = Sim_local.perday(d).par;
    Sim_local.perday(d).scenopt.res  = Sim_local.perday(d).res;
end
Sim_local.perday(d).end.CHP_run=Sim_local.perday(d).res.S_run_CHP_T(Sim_local.perday(d).par.T_C); %Endstatus des CHP runs wird mit uebernommen
Sim_local.perday(d).t_sim.end      = datetime('now');
Sim_local.TIME.t_sim_end=             Sim_local.perday(d).t_sim.end;
Sim_local.perday(d).t_sim.status   = Sim_local.perday(d).cplex.Solution.statusstring;
%% Zuordnung abspeichern der Ergebnisse
%hier werden die KPIs der einzelnen
%Tagessimulation gebildet
[Sim_local.perday(d).KPI, Sim_local.perday(d).TIME] = KPICalc( Sim_local.perday(d).res, Sim_local.perday(d).I_TC, Sim_local.perday(d).I, par, opt, 1 );
[Sim_local.perday(d).scenopt.KPI]                   = KPICalc( Sim_local.perday(d).scenopt.res, Sim_local.perday(d).I_TC, Sim_local.perday(d).I, par, opt, 1 );
end